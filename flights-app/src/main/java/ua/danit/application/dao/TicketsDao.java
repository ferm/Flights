package ua.danit.application.dao;


import ua.danit.application.model.Flight;
import ua.danit.application.model.Tickets;
import ua.danit.framework.utils.EntityUtils;


public class TicketsDao {

    public Iterable<Tickets> getTopTickets(int limit) {
        String sql = "select ID as id, `CUSTOMER_ID` as `customer_id`, " +
                " `DESTINATION_CITY` as `to`," +
                "`DEPARTURE_CITY` as `from`," +
                "from TICKETS";
        return EntityUtils.nativeQuery(sql, Tickets.class);
    }


    public Iterable<Tickets> searchTickets(Long id) {
        if (id != null) {
            String sql = "select ID as id, `CUSTOMER_ID` as `customer_id`, " +
                    " `DESTINATION_CITY` as `to`," +
                    "`DEPARTURE_CITY` as `from`," +
                    "from TICKETS" +
                    "where `DEPARTURE_CITY` like'" + id + "%' " +
                    "or `DESTINATION_CITY` like '" + id + "%'";
            return EntityUtils.nativeQuery(sql, Tickets.class);
        }
        return null;
    }

}
